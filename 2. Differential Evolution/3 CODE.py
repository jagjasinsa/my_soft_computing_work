# -*- coding: utf-8 -*-
"""
Created on Fri Jan 20 19:56:57 2017"""

#Differntial evolution
#function to be minimised using Diffential evolution is sphere function y = x1*x1 + x2*x2 + x3*x3

import numpy as num
from random import random
import random as rand
import math
import matplotlib.pyplot as plt
import warnings
warnings.filterwarnings('ignore')

def check_for_inconsistency(arr,popsize):
    for i in range(0,popsize):
        if arr[i][0]<-5.12 or arr[i][0]>5.12:
            arr[i][0] = (rand.random()-0.5)*10.24
    return arr



def compute_gen(target_vector1,target_vector2,target_vector3,population_size): 

    mutant_vector1 = num.zeros(shape = (int(population_size),1));
    mutant_vector2 = num.zeros(shape = (int(population_size),1));
    mutant_vector3 = num.zeros(shape = (int(population_size),1));

    for i in range(0,int(population_size)): 
        three_chosen = rand.sample(range(0,int(population_size)),3)
    #print(three_chosen)
        mutant_vector1[i,0] = target_vector1[three_chosen[0],0] + 0.85*(target_vector1[three_chosen[1],0]-target_vector1[three_chosen[2],0])
        mutant_vector2[i,0] = target_vector2[three_chosen[0],0] + 0.85*(target_vector2[three_chosen[1],0]-target_vector2[three_chosen[2],0])
        mutant_vector3[i,0] = target_vector3[three_chosen[0],0] + 0.85*(target_vector3[three_chosen[1],0]-target_vector3[three_chosen[2],0])
    
    mutant_vector1 = check_for_inconsistency(mutant_vector1,int(population_size))
    mutant_vector2 = check_for_inconsistency(mutant_vector2,int(population_size))
    mutant_vector3 = check_for_inconsistency(mutant_vector3,int(population_size))

        
    trial_vector1 = num.zeros(shape = (int(population_size),1));
    trial_vector2 = num.zeros(shape = (int(population_size),1));
    trial_vector3 = num.zeros(shape = (int(population_size),1));

    #CR=0.5
    for i in range(0,int(population_size)):
        a = random()
        if a>0.5:
            trial_vector1[i,0] = target_vector1[i,0]
        else:
            trial_vector1[i,0] = mutant_vector1[i,0]

    for i in range(0,int(population_size)):
        a = random()
        if a>0.5:
            trial_vector2[i,0] = target_vector2[i,0]
        else:
            trial_vector2[i,0] = mutant_vector2[i,0]

    for i in range(0,int(population_size)):
        a = random()
        if a>0.5:
            trial_vector3[i,0] = target_vector3[i,0]
        else:
            trial_vector3[i,0] = mutant_vector3[i,0]

    for i in range(0, int(population_size)):
        x1 = trial_vector1[i,0]
        y1 = target_vector1[i,0]
        x2 = trial_vector2[i,0]
        y2 = target_vector2[i,0]
        x3 = trial_vector3[i,0]
        y3 = target_vector3[i,0]
        z1 = x1**2 + x2**2 + x3**2
        z2 = y1**2 + y2**2 + y3**2
        if z1 > z2:
            trial_vector1[i,0] = target_vector1[i,0]
            trial_vector2[i,0] = target_vector2[i,0]
            trial_vector3[i,0] = target_vector3[i,0]

    y1 = num.random.randint(2,size = (int(population_size),1))
    for i in range(0,int(population_size)):
        a = trial_vector1[i,0]
        b = trial_vector2[i,0]
        c = trial_vector3[i,0]
        y1[i,:] = a**2 + b**2 + c**2

    return trial_vector1,trial_vector2,trial_vector3,y1
        
        





population_size = input('Mention the size of the population \n')
'''print(population_size)'''

target_vector1 = (num.random.rand(int(population_size),1)-0.5)*10.24
target_vector2 = (num.random.rand(int(population_size),1)-0.5)*10.24
target_vector3 = (num.random.rand(int(population_size),1)-0.5)*10.24

'''target_vector1 = num.random.randint(2,size = (int(population_size),int(chromosome_size)))
target_vector2 = num.random.randint(2,size = (int(population_size),int(chromosome_size)))
target_vector3 = num.random.randint(2,size = (int(population_size),int(chromosome_size)))'''

gen11,gen12,gen13,output1 =compute_gen(target_vector1,target_vector2,target_vector3,population_size)
'''gen21,gen22,gen23,output2 =compute_gen(gen11,gen12,gen13,population_size,chromosome_size)
gen31,gen32,gen33,output3 =compute_gen(gen21,gen22,gen23,population_size,chromosome_size)
gen41,gen42,gen43,output4 =compute_gen(gen31,gen32,gen33,population_size,chromosome_size)
gen51,gen52,gen53,output5 =compute_gen(gen41,gen42,gen43,population_size,chromosome_size)
gen61,gen62,gen63,output6 =compute_gen(gen51,gen52,gen53,population_size,chromosome_size)
gen71,gen72,gen73,output7 =compute_gen(gen61,gen62,gen63,population_size,chromosome_size)
gen81,gen82,gen83,output8 =compute_gen(gen71,gen72,gen73,population_size,chromosome_size)

x_axis = num.zeros(shape = (1,int(chromosome_size)));
for i in range(0,int(chromosome_size)):
    x_axis[0,i] = i+1




plt.plot(output1,'r-',label='1st Gen')
plt.plot([0,population_size], [num.mean(output1), num.mean(output1)], color='r', linestyle='--', linewidth=1)
plt.plot([0,population_size], [num.mean(output2), num.mean(output2)], color='b', linestyle='--', linewidth=1)
plt.plot(output2,'b-',label = '2nd Gen')
plt.plot([0,population_size], [num.mean(output8), num.mean(output8)], color='y', linestyle='--', linewidth=1)
plt.plot(output8,'y-',label = '8th Gen')
plt.legend()
plt.show()'''

for zz in range(1,1000):
    gen21,gen22,gen23,output2 =compute_gen(gen11,gen12,gen13,population_size)
    gen11,gen12,gen13 =  gen21,gen22,gen23
    print(zz)
    
plt.plot(output1,'r-',label='1st Gen')
plt.plot([0,population_size], [num.mean(output1), num.mean(output1)], color='r', linestyle='--', linewidth=1)
plt.plot([0,population_size], [num.mean(output2), num.mean(output2)], color='b', linestyle='--', linewidth=1)
plt.plot(output2,'b-',label = 'Last Gen')
plt.plot([-1])
plt.legend()
plt.show()

