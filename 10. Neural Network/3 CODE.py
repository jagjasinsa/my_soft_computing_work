import numpy as num
import random
import math


input_iterations = 10000
hidden_layer_size = 10
learning_rate = 0.001

range_a = 1
range_b = 10


input_layer = num.zeros(shape = (input_iterations,2),dtype = int)
for i in range(0,input_iterations):
    input_layer[i,0] = random.randint(range_a,range_b)
    input_layer[i,1] = random.randint(range_a,range_b)

output_layer = num.zeros(shape = (input_iterations,1),dtype = int)

for i in range(0,input_iterations):
    output_layer[i,0] = input_layer[i,0] + input_layer[i,1]


weight1 = (num.random.rand(2,hidden_layer_size)-0.5)*2 #2xh
weight2 = (num.random.rand(hidden_layer_size,1)-0.5)*2 #hx1
hidden_layer = num.zeros(shape = (hidden_layer_size,1)) #hx1

#print(input_layer)
for i in range(0,input_iterations):
    #Feedforward
    alpha = input_layer[i]
    for j in range(0,hidden_layer_size):
        hidden_layer[j,0] = alpha[0]*weight1[0,j] + alpha[1]*weight1[1,j]

    hidden_layer = 1/(1+math.e**(-hidden_layer))

    beta = 0
    for k in range(0,hidden_layer_size):
        beta = beta + hidden_layer[k,0]*weight2[k][0]

    beta = 1/(1+math.e**(-beta))

    error = output_layer[i,0]/(2*range_b) - beta

    #Backpropagation
    delta_beta = beta*(1-beta)*error
    weight2 = weight2 + learning_rate*beta*delta_beta

    delta_alpha = num.zeros(shape = (hidden_layer_size,1))
    for k in range(0,hidden_layer_size):
        delta_alpha[k,0] =  hidden_layer[k,0]*(1-hidden_layer[k,0])*(delta_beta*weight2[k,0])

    for k in range(0,hidden_layer_size):
        weight1[0,k] = weight1[0,k] + learning_rate*alpha[0]*delta_alpha[k,0]
        weight1[1,k] = weight1[1,k] + learning_rate*alpha[1]*delta_alpha[k,0]



alpha[0] = int(input('NUM A:: '))
alpha[1] = int(input('NUM B:: '))

for j in range(0,hidden_layer_size):
    hidden_layer[j,0] = alpha[0]*weight1[0,j] + alpha[1]*weight1[1,j]

hidden_layer = 1/(1+math.e**(-hidden_layer))

beta = 0
for k in range(0,hidden_layer_size):
    beta = beta + hidden_layer[k,0]*weight2[k][0]

beta = 1/(1+math.e**(-beta))
beta = beta*20
print('SUM IS: '+str(beta))
