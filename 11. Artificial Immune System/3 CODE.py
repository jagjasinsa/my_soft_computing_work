import numpy as num
import random
import matplotlib.pyplot as plt


def find_gbest():
    global antibodies
    global population_size
    
    a = num.sin(antibodies)
    b = num.amax(a)
    for i in range(0,population_size):
        if a[i,0] == b:
            return antibodies[i,0]
    return 0

def ais(antibodies):
    
    global population_size
    global eta
    
    gbest = find_gbest()
    gbest = round(gbest,2)
    clonal_antibodies = num.zeros(shape = (population_size,1))
    clonal_antibodies[:,0] = gbest
    
    termination_of_clonal = int(0.8*population_size)
    for i in range(0,termination_of_clonal):
        r = random.random()
        if  r <= 0.5:
            clonal_antibodies[i,0] += (2*r)**(1/(eta + 1)) - 1
        else:
            clonal_antibodies[i,0] += 1 - (2*(1-r))**(1/(eta + 1))
            
    appended_array = num.append(antibodies,clonal_antibodies)
    value_array = num.sin(appended_array)
    value_array = num.sort(value_array)
    value_array = value_array[::-1]
    
    new_antibodies = num.zeros(shape = (population_size,1))
    
    for i in range(0,population_size):
        for j in range(0,2*population_size):
            if value_array[i] == num.sin(appended_array[j]):
                new_antibodies[i,0] = appended_array[j]
    return new_antibodies


population_size = input('Please mention the population size: ')
population_size = int(population_size)
eta = 0.3
antibodies = (num.random.rand(population_size,1)-0.5)*10
plt.plot(num.sin(antibodies),label='1st Gen')

for i in range(0,1000):
    print('Iteration number: '+ str(i))
    antibodies = ais(antibodies)

plt.plot(num.sin(antibodies),label='Last Gen')
plt.legend()
plt.show()

        



