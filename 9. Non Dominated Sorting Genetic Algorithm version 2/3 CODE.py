# -*- coding: utf-8 -*-
"""
Spyder Editor
"""

#f1 = x**2
#f2 = (x-2)**2
#x belongs to [-5,5]

import numpy as num
import random
import math
import matplotlib.pyplot as plt


    

def compute_rank(functional_value,population_size,num_of_func):
    
    s = []
    f = []
    rank_matrix = num.zeros(shape = (population_size,1))
    fftemp = []
    n = num.zeros(shape = (population_size,1))
    for p in range(0,population_size):
        qqtemp = []
        for q in range(0,population_size):
            if p!=q:
                if functional_value[p,0]<functional_value[q,0] and functional_value[p,1]<functional_value[q,1]:
                    qqtemp.append(q)
                elif functional_value[p,0]>functional_value[q,0] and functional_value[p,1]>functional_value[q,1]:
                    n[p] = n[p] + 1
        s.append(qqtemp)
        if n[p]==0:
            rank_matrix[p]=1
            fftemp.append(p)
          
    f.append(fftemp)  
    #print(f)
    #print(n)
    #print(s)
    
    i=0
    j=0
    while len(f[i])!=0:
        h=[]
        for pp in range(0,len(f[i])):
            p = f[i][pp]
            for qq in range(0,len(s[p])):
                q = s[p][qq]
                n[q] = n[q] - 1
                if n[q]==0:
                    rank_matrix[q]=i+2
                    h.append(q)
        i = i + 1
        if len(h)!=0:
            f.append(h)
        else:
            break
         
         
    #print(f)
    #print(rank_matrix)
    
    return f,rank_matrix
 


def crowding_distance(x):    
    l = len(x)    
    x2 = num.zeros(shape = (l,4))
    x2[:,0] = x[:,0]
    x2[:,1] = x[:,0]**2
    x2[:,2] = (x[:,0]-2)**2
    x2[x2[:,1].argsort()]
    x2[0,3] = math.inf 
    x2[l-1,3] = math.inf 
    #print(x2)
    for i in range(1,l-1):
        x2[i,3] = x2[i,3] + (x2[i+1,1]-x2[i-1,1])/(x2[l-1,1]-x2[0,1])
        
    
    x2[x2[:,2].argsort()]
    x2[0,3] = math.inf 
    x2[l-1,3] = math.inf 
    for i in range(1,l-1):
        x2[i,3] = x2[i,3] + (x2[i+1,2]-x2[i-1,2])/(x2[l-1,2]-x2[0,2]) 
        
    '''for i in range(0,l):
        if x2[i,3]>1000000:
            x2[i,3]=1000000'''
              
          
    return x2



def compute_final_matrix(chromosome,rank_matrix,f,population_size1):
    final_matrix = num.zeros(shape = (population_size1,3))
    final_matrix[:,0] = chromosome[:,0]
    final_matrix[:,1] = rank_matrix[:,0]
    
    for i in range(0,len(f)):
        x = num.zeros(shape = (len(f[i]),1))
        for j in range(0,len(f[i])):
            x[j,0] = chromosome[f[i][j]]
            
        x2 = crowding_distance(x)
        #print(x2[:,3])
        for ii in range(0,population_size1):
            for jj in range(0,len(x2[:,0])):
                if chromosome[ii,0] == x2[jj,0]:
                    final_matrix[ii,2] = x2[jj,3]
                    
                    
    return final_matrix

def selection(selection_ratio,final_matrix,population_size):
    parent_matrix = num.zeros(shape = (selection_ratio,1))    
    selected_num = []
    i=0
    while i!=selection_ratio:
        r1 = random.randint(0,population_size-1)
        r2 = random.randint(0,population_size-1)
        if r1!=r2:
            if final_matrix[r1,1] < final_matrix[r2,1] and r1 not in selected_num:
                parent_matrix[i] = final_matrix[r1,0]
                selected_num.append(r1)
                #print(i,'alpha')
                i+=1
            elif final_matrix[r1,1] > final_matrix[r2,1] and r2 not in selected_num:
                parent_matrix[i] = final_matrix[r2,0]
                selected_num.append(r2)
                #print(i,'beta')
                i+=1
            if final_matrix[r1,1] == final_matrix[r2,1]:
                if final_matrix[r1,2] > final_matrix[r2,2] and r1 not in selected_num:
                    parent_matrix[i] = final_matrix[r1,0]
                    selected_num.append(r1)
                    #print(i,'gamma')
                    i+=1
                elif final_matrix[r1,2] < final_matrix[r2,2] and r2 not in selected_num:
                    parent_matrix[i] = final_matrix[r2,0]
                    selected_num.append(r2)
                    #print(i,'delta')
                    i+=1
                else:
                    pass


                    
    #print(parent_matrix)               
    return parent_matrix

def crossover(selection_ratio,parent_matrix):
    child_matrix = num.zeros(shape = (selection_ratio,1))    
    for i in range(0,selection_ratio,2):
        b = 0
        r = random.random()
        if r <= 0.5:
            numb = 2*r
            powz = round(1/(1+mu),3)
            b = numb**powz
        else:             
            numb = 1/(2*(1-r))
            powz = 1/(1+mu)
            b = numb**powz
        
        child_matrix[i,0] = 0.5*((1+b)*parent_matrix[i] + (1-b)*parent_matrix[i+1])
        if child_matrix[i,0]>5 or child_matrix[i,0]<-5:
            c = (random.random()-0.5)*10
            round(c,2)
            child_matrix[i,0] = c
        child_matrix[i+1,0] = 0.5*((1+b)*parent_matrix[i+1] + (1-b)*parent_matrix[i])
        if child_matrix[i+1,0]>5 or child_matrix[i+1,0]<-5:
            c = (random.random()-0.5)*10
            round(c,2)
            child_matrix[i+1,0] = c
        
    return child_matrix


def mutation(selection_ratio,child_matrix,parent_matrix):
    mutation_ratio = round(selection_ratio*0.1)
    i = mutation_ratio
    if mutation_ratio!=0:
        while i!=0:
            r = random.randint(0,selection_ratio-1)
            ra = random.random()
            if ra <= 0.5:
                numb = 2*ra
                powz = 1/(1+mu)
                b = numb**powz - 1
            else:             
                numb = 1/(2*(1-ra))
                powz = 1/(1+mu)
                b = 1 - (numb**powz)
            child_matrix[r,0] = parent_matrix[r,0] + b
            if child_matrix[r,0]>5 or child_matrix[r,0]<-5:
                 c = (random.random()-0.5)*10
                 round(c,2)
                 child_matrix[r,0] = c
            i = i - 1
    
    return child_matrix


def compute_gen(population_size,chromosome,num_of_func,mu): 
    #print('start')
    functional_value = num.zeros(shape = (population_size,num_of_func));
    functional_value[:,0] = chromosome[:,0]**2
    functional_value[:,1] = (chromosome[:,0]-2)**2
    f,rank_matrix = compute_rank(functional_value,population_size,num_of_func)
    
    #FINAL MATRIX WITH X, RANK, CROWDING DISTANCE 
    final_matrix = compute_final_matrix(chromosome,rank_matrix,f,population_size)
    final_matrix = num.round(final_matrix, decimals=2)
    #print(final_matrix)                   
    
    
    #SELECTION
    selection_ratio = round(population_size*0.8)
    parent_matrix = selection(selection_ratio,final_matrix,population_size)
    #print(parent_matrix)

    #CROSSOVER    
    child_matrix = crossover(selection_ratio,parent_matrix)
    #print(child_matrix)
        
    #MUTATION
    child_matrix = mutation(selection_ratio,child_matrix,parent_matrix)
            
    #thus this is the total population (1.8*population_size) 
    new_matrix_size = selection_ratio + population_size       
    new_matrix = num.zeros(shape = (new_matrix_size,1))
    j=0
    for i in range(0,population_size):
        new_matrix[j,0] = chromosome[i,0]
        j = j + 1
        
    for i in range(0,selection_ratio):
        new_matrix[j,0] = child_matrix[i,0]
        j = j + 1
        
    functional_value = num.zeros(shape = (new_matrix_size,num_of_func));
    functional_value[:,0] = new_matrix[:,0]**2
    functional_value[:,1] = (new_matrix[:,0]-2)**2   
    f,rank_matrix = compute_rank(functional_value,new_matrix_size,num_of_func)
    final_matrix2 = compute_final_matrix(new_matrix,rank_matrix,f,new_matrix_size)
    
    #print(final_matrix2)
    final_matrix2 = final_matrix2[num.lexsort((-final_matrix2[:,2],final_matrix2[:,1]))]   
    #print(final_matrix2)
    
    #print(len(final_matrix2))
    for i in range(0,population_size):
        chromosome[i,0] = final_matrix2[i,0]
    
    #print(chromosome)'''
    return chromosome,f
      
      
population_size = input("Please mention the population size\n")
population_size = int(population_size)

chromosome = num.zeros(shape = (population_size,1))
for i in range(0,population_size):
    chromosome[i,0] = (random.random()-0.5)*10
                            
'''chromosome[0,0] = -0.414
chromosome[1,0] =  0.467
chromosome[2,0] = 0.818         
chromosome[3,0] = 1.735          
chromosome[4,0] = 3.210
chromosome[5,0] = -1.272
chromosome[6,0] = -1.508
chromosome[7,0] = -1.832
chromosome[8,0] = -2.161
chromosome[9,0] = -4.105''' 
          
mu = random.random()
mu = round(mu,2)
          
num_of_func = int(2)


plt.figure()

f1 = chromosome**2
f2 = (chromosome-2)**2    
plt.scatter(f1,f2, label = '1st gen')
plt.legend() 

i = 100
while 1:    
    chromosome,f = compute_gen(population_size,chromosome,num_of_func,mu)
    print(len(f))
    if len(f)<=3:
        break
    #chromosome = chromosome1
    #i = i - 1
    
f1 = chromosome**2
f2 = (chromosome-2)**2

plt.figure()   
plt.scatter(f1,f2, label = 'Last gen')
plt.legend() 
plt.show()






                         

