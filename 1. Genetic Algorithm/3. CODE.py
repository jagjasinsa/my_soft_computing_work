#  -*- coding: utf-8 -*-
"""
BINARY_GA IMPLEMENTATION AS MENTIONED IN THE CLASS.
"""

#function is sin(x)
import numpy as num
from random import random
import math
import matplotlib.pyplot as plt
import warnings
warnings.filterwarnings('ignore')
    
def value_of_chromosome(arr,c,pos):
    chromosome_size = int(c)
    x = 0
    for i in range(1,chromosome_size):
        x = x + (2**i)*int(arr[pos,i])
    return x
 
def generate_fit_solution(array,population_size,chromosome_size):
    x = random()*float(population_size)
    y = random()*float(population_size)
    a = value_of_chromosome(array,chromosome_size,x)
    b = value_of_chromosome(array,chromosome_size,y)
    if(a>b): return x
    else: return y
    
def generate_generation(population_size,chromosome_size,array):

    #Finding the fitness based solutions, taking fitness percentage 80
    f = float(population_size)*0.8
    fitness_size = math.floor(f)
    fitness_array = []
    while(len(fitness_array)<fitness_size):
        a = int(generate_fit_solution(array,population_size,chromosome_size))
        if(a==0): a = a + 1
        if(a not in fitness_array):
            fitness_array.append(a)
            #print(fitness_array)
            
            #Finding children matrix from crossover   
            children_array =   num.zeros(shape = (int(fitness_size),int(chromosome_size)))
            #print(children_array)
            for i in range(0,len(fitness_array)-1,2):
                a = fitness_array[i]
                b = fitness_array[i+1]
                crossover_point = int(random()*10)+1
                #print(crossover_point)
                for j in range(1,crossover_point):
                    children_array[i,j] = array[b,j]
                    children_array[i+1,j] = array[a,j]
                for j in range(crossover_point,int(chromosome_size)):
                    children_array[i,j] = array[a,j]
                    children_array[i+1,j] = array[b,j]
    
    
    #finding combined matrix of children, parent and bachelors and sort
    finale_array = num.concatenate((array,children_array),axis=0)
    value_of_finale_array = num.zeros(shape = (int(len(finale_array)),2))
    #print(value_of_finale_array)
    for i in range(0,len(finale_array)):
        value_of_finale_array[i,0] = i+1
        value_of_finale_array[i,1] = math.sin(value_of_chromosome(finale_array,chromosome_size,i))
        value_of_finale_array = value_of_finale_array[num.argsort(value_of_finale_array[:, 1])[::-1]]
    #print(finale_array)
    #print()
    '''print(value_of_finale_array)
    print()
    for i in range(0,len(value_of_finale_array)):
    print(value_of_finale_array[i][0])'''
    #print(array.shape)
    #print(children_array.shape)
    #print(value_of_finale_array.shape)
    
    #computing the first generation population
    final_array = num.zeros(shape = (int(population_size),int(chromosome_size)))
    chromosome_to_be_selected = [row[0] for row in value_of_finale_array]
    chromosome_to_be_selected = num.int_(chromosome_to_be_selected)
    #print(chromosome_to_be_selected)
    for i in range (0,int(population_size)):
        zz = chromosome_to_be_selected[i]-1
        final_array[i,:] = finale_array[zz,:]
        #print(finale_array[chromosome_to_be_selected[i-1]])
        
        #Use this to see what is the next generation output    
        '''for i in range(0,int(population_size)):
            print(math.sin(value_of_chromosome(final_array,chromosome_size,i)))'''
    return final_array
    

population_size = input('Mention the size of the population \n')
'''print(population_size)'''
chromosome_size = input('Mention the size of the chromosome \n')
#Defining the structure of the population.
array = num.random.randint(2,size = (int(population_size),int(chromosome_size)))
#print(array)
output1 = []
generation1 = generate_generation(population_size,chromosome_size,array)
for i in range(0,int(population_size)):
            output1.append(math.sin(value_of_chromosome(generation1,chromosome_size,i)))

output2 = []
generation2 = generate_generation(population_size,chromosome_size,generation1)
for i in range(0,int(population_size)):
            output2.append(math.sin(value_of_chromosome(generation2,chromosome_size,i)))
            
output3 = []
generation3 = generate_generation(population_size,chromosome_size,generation2)
for i in range(0,int(population_size)):
            output3.append(math.sin(value_of_chromosome(generation3,chromosome_size,i)))
            
output4 = []
generation4 = generate_generation(population_size,chromosome_size,generation3)
for i in range(0,int(population_size)):
            output4.append(math.sin(value_of_chromosome(generation4,chromosome_size,i)))
            
            
plt.figure(1)
plt.subplot(221)
plt.plot(output1)
plt.plot([0,population_size], [num.mean(output1), num.mean(output1)], color='r', linestyle='--', linewidth=1)
plt.subplot(222)
plt.plot(output2)
plt.plot([0,population_size], [num.mean(output2), num.mean(output2)], color='r', linestyle='--', linewidth=1)
plt.subplot(223)
plt.plot(output3)
plt.plot([0,population_size], [num.mean(output3), num.mean(output3)], color='r', linestyle='--', linewidth=1)
plt.subplot(224)
plt.plot(output4)
plt.plot([0,population_size], [num.mean(output4), num.mean(output4)], color='r', linestyle='--', linewidth=1)
plt.show()
