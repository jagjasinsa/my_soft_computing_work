import matplotlib.pyplot as plt
import numpy as num

#Defining temperature
# temperature: {freezing, cool, warm, hot}
def freezing(x):
    y = 0
    if x >= 0 and x <= 30:
        y = 1
    elif x > 30 and x <= 50:
        y = (-0.05*x)+2.5
    return round(y,2)

def cool(x):
    y = 0
    if x >=30  and x <=50:
        y = 0.05*x - 1.5
    elif x > 50 and x <= 70:
        y = (-0.05*x) + 3.5
    return round(y,2)

def warm(x):
    y = 0
    if x >=50  and x <=70:
        y = 0.05*x - 2.5
    elif x > 70 and x <= 90:
        y = (-0.05*x) + 4.5
    return round(y,2)

def hot(x):
    y = 0
    if x >= 70 and x <= 90:
       y = 0.05*x - 3.5
    elif x > 90:
        y = 1
    return round(y,2)

#Defining cover
#cover: {sunny, partly, overcast}
def sunny(x):
    y = 0
    if x >= 0 and x <= 20:
        y = 1
    elif x > 20 and x <= 40:
        y = (-0.05*x)+2
    return round(y,2)

def partly(x):
    y = 0
    if x >=20  and x <=50:
        y = (x - 20)/30
    elif x > 50 and x <= 80:
        y = -(x - 80)/30
    return round(y,2)

def overcast(x):
    y = 0
    if x >= 60 and x <= 80:
       y = (x-60)/20
    elif x > 80:
        y = 1
    return round(y,2)

#Defining speed
#speed: {slow, medium, fast}
def slow(x):
    y = 0
    if x >= 0 and x <= 20:
        y = 1
    elif x > 20 and x <= 40:
        y = (-0.05*x)+2
    return round(y,2)

def medium(x):
    y = 0
    if x >=20  and x <=50:
        y = (x - 20)/30
    elif x > 50 and x <= 80:
        y = -(x - 80)/30
    return round(y,2)

def fast(x):
    y = 0
    if x >= 60 and x <= 80:
       y = (x-60)/20
    elif x > 80:
        y = 1
    return round(y,2)


#Rules
def rules(a):
    #0: slow
    #1: medium
    #2: fast
    output = []
    if a[0,4]!=0 and a[0,3]!=0:
        output.append(2)
        output.append(max(a[0,4],a[0,3]))
    if a[0,4]!=0 and a[0,2]!=0:
        output.append(2)
        output.append(max(a[0,4],a[0,2]))
    if a[0,4]!=0 and a[0,1]!=0:
        output.append(1)
        output.append(max(a[0,4],a[0,1]))
    if a[0,4]!=0 and a[0,0]!=0:
        output.append(1)
        output.append(max(a[0,4],a[0,0]))
    if a[0,5]!=0 and a[0,3]!=0:
        output.append(2)
        output.append(max(a[0,5],a[0,3]))
    if a[0,5]!=0 and a[0,2]!=0:
        output.append(1)
        output.append(max(a[0,5],a[0,2]))
    if a[0,5]!=0 and a[0,1]!=0:
        output.append(0)
        output.append(max(a[0,5],a[0,1]))
    if a[0,5]!=0 and a[0,0]!=0:
        output.append(0)
        output.append(max(a[0,5],a[0,0]))
    if a[0,6]!=0 and a[0,3]!=0:
        output.append(1)
        output.append(max(a[0,6],a[0,3]))
    if a[0,6]!=0 and a[0,2]!=0:
        output.append(0)
        output.append(max(a[0,6],a[0,2]))
    if a[0,6]!=0 and a[0,1]!=0:
        output.append(0)
        output.append(max(a[0,6],a[0,1]))
    if a[0,6]!=0 and a[0,0]!=0:
        output.append(0)
        output.append(max(a[0,6],a[0,0]))
        
    return output
    



temp = input("Please mention the temperature ")
cov = input("Please mention the cloud cover in percentage ")
temp = int(temp)
cov = int(cov)

a = num.zeros(shape = (1,7))
#In a, it is of the form [freezing, cool, warm, hot, sunny, partly, overcast]

a[0,0] = freezing(temp)
a[0,1] = cool(temp)
a[0,2] = warm(temp)
a[0,3] = hot(temp)
a[0,4] = sunny(cov)
a[0,5] = partly(cov)
a[0,6] = overcast(cov)
#print(a)
output = rules(a)
#print(output)
num = 0
den = 0
for i in range(0,len(output),2):
    k=0
    if output[i]==0:
        k = 20
    elif output[i]==1:
        k = 50
    else:
        k = 80
        
    num = num + k*output[i+1]
    den = den + output[i+1]
    
answer = num/den
print("The speed is: ")
print(round(answer,2))

plt.title('Temperature')
aa = [0,30,50]
bb = [1,1,0]
plt.plot(aa,bb)
aa = [30,50,70]
bb = [0,1,0]
plt.plot(aa,bb)
bb = [0,1,1]
aa = [70,90,120]
plt.plot(aa,bb)
aa = [50,70,90]
bb = [0,1,0]
plt.plot(aa,bb)

plt.figure()
plt.title('Cover')
aa = [0,20,40]
bb = [1,1,0]
plt.plot(aa,bb)
aa = [20,50,80]
bb = [0,1,0]
plt.plot(aa,bb)
aa = [60,80,100]
bb = [0,1,1]
plt.plot(aa,bb)

plt.figure()
plt.title('Speed')
aa = [0,20,40]
bb = [1,1,0]
plt.plot(aa,bb)
aa = [20,50,80]
bb = [0,1,0]
plt.plot(aa,bb)
aa = [60,80,100]
bb = [0,1,1]
plt.plot(aa,bb)
plt.show()


    
        
    
    
    




