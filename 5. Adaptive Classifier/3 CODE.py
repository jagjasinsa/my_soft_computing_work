# -*- coding: utf-8 -*-
"""
Created on Mon Feb  6 22:17:26 2017

@author: Jagpreet Singh
"""
import numpy as num
import math
import matplotlib.pyplot as plt

truth_table = num.array([[0.1,0.1,0.1],[0.1,0.9,0.9],[0.9,0.1,0.9],[0.9,0.9,0.1]])
weights_x =  num.zeros(shape=(3,1))
weights_y = num.zeros(shape=(3,1))
x_input = num.zeros(shape=(3,1))
y_input = num.zeros(shape=(3,1))

iteration = 0
pii = num.round((math.pi),2)
counter = 0
mu=0.1
error_pattern = []

while(1):
    counter = counter + 1
    x_input[0] = truth_table[iteration%4,0]
    x_input[1] = math.sin(truth_table[iteration%4,0])
    x_input[2] = math.cos(truth_table[iteration%4,0])
    y_input[0] = truth_table[iteration%4,1]
    y_input[1] = math.sin(truth_table[iteration%4,1])
    y_input[2] = math.cos(truth_table[iteration%4,1])
    
    weighted_output_x = x_input*weights_x
    weighted_output_y = y_input*weights_y
    
    summation = num.sum(weighted_output_x) + num.sum(weighted_output_y)
    
    expected_output = truth_table[iteration%4,2]
    
    error = expected_output - summation
    error_square = error**2
    error_pattern.append(error_square)
    
    weights_x = weights_x + 2*mu*x_input*error
    weights_y = weights_y + 2*mu*y_input*error
    
    iteration = iteration + 1
        
    if counter == 100:
        break
size = num.size(error_pattern)
x = num.arange(size) 
#print(x_input,y_input)
#a = num.arange(size)   
 
plt.plot(x,error_pattern)   
#print(error_pattern)
plt.show()
    
    
